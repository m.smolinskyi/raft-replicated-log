export interface HeartBeatResponseDto {
    success: boolean;
    term: number;
    commitIndexs?: number[]
}