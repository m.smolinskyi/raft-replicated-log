export interface SubmitMsgDto {
    msg: string;
    w: number;
}