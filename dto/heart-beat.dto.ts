export interface HeartBeatDto {
    from: string;
    term: number;
    leaderId: string;
}