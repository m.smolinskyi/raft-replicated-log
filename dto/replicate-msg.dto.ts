export interface ReplicateMsgDto {
  msg: string;
  commitIndex: number;
}
