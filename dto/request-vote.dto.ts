export interface RequestVoteDto {
  term: number;
  candidateId: string;
}
