import express from "express";
import { RaftNode } from "./raft/node";
import type { RequestVoteDto } from "./dto/request-vote.dto";
import type { HeartBeatDto } from "./dto/heart-beat.dto";
import { NodeType } from "./raft/node-type.enum";
import type { SubmitMsgDto } from "./dto/submit-msg.dto";
import type { ReplicateMsgDto } from "./dto/replicate-msg.dto";
import type { HeartBeatResponseDto } from "./dto/heart-beat-response.dto";

const app = express();
app.use(express.json());

const PORT = process.argv[2];
const nodeId = PORT;

const raftNode = new RaftNode(nodeId);



// leader EP
app.get("/hc", (_, res) => {
  res.status(200);
  res.send();
});

// follower EP
app.post("/hb", (req, res) => {
  if (raftNode.type === NodeType.leader) {
    return res.status(404).json({ message: "not found" });
  }

  const { from }: HeartBeatDto = req.body;

  console.log(`[LOG]: Received HeartBeat from ID ${from}`);

  raftNode.leader = from;
  raftNode.timer.reset();
  res.json({
    term: raftNode.term,
    success: true,
    commitIndexs: raftNode.commitIndexs,
  } as HeartBeatResponseDto);
});

// leader EP
app.get("/msg", (_, res) => {
  console.log("[LOG]: Receiving messages from state");
  if (raftNode.isFollower && raftNode.leader) {
    try {
      return res.redirect(`http://localhost:${raftNode.leader}/msg`);
    } catch (error) {
      res.status(500);
      res.json({ error });
    }
  }

  res.json(raftNode.messages);
});

app.post("/msg", async (req, res) => {
  const { msg, w }: SubmitMsgDto = req.body;

  await raftNode.submitMsg(msg, w);

  res.json(raftNode.messages);
});

app.post("/replicateMsg", async (req, res) => {
  const { msg, commitIndex }: ReplicateMsgDto = req.body;

  raftNode.replicateMsg(msg, commitIndex);

  res.json({ term: raftNode.term, success: true });
});

app.post("/requestVote", async (req, res) => {
  const { term, candidateId }: RequestVoteDto = req.body;

  raftNode.timer.stop();

  if (
    term >= raftNode.term &&
    (raftNode.votedFor === null || raftNode.votedFor === candidateId)
  ) {
    return res.json({ term: term, voteGranted: true });
  }

  return res.json({ term: term, voteGranted: false });
});

app.listen(PORT, () => {
  console.log(`[LOG]: Server started on port: ${PORT}`);
});
