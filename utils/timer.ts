import EventEmitter from "events";

import isNil from 'lodash/fp/isNil'

export class Timer extends EventEmitter {
  timerId: ReturnType<typeof setInterval> | null = null;
  randTime = 0;
  timeDelta = 300;

  constructor() {
    super();
  }

  start(randTime: number = getTimeout()) {
    this.randTime = randTime;
    this.timerId = setInterval(() => {
      this.emit("timeout");
    }, randTime);
  }

  stop() {
    !isNil(this.timerId) && clearInterval(this.timerId);
  }

  reset() {
    this.stop();
    this.randTime += this.timeDelta;
    this.start(this.randTime);
  }
}

export function getTimeout() {
  return getRandomIntInclusive(5000, 10000);
}

export function getRandomIntInclusive(min: number, max: number) {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1) + min);
}
