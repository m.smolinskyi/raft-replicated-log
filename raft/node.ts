import { NodeType } from "./node-type.enum";

import { Timer } from "../utils/timer";
import { PEERS, HEARTBEAT_TIME } from "../constants";
import axios, { type AxiosResponse } from "axios";

import type { RequestVoteDto } from "../dto/request-vote.dto";
import type { HeartBeatDto } from "../dto/heart-beat.dto";
import { isNumber, last, size, differenceBy, isNil } from "lodash";
import type { ReplicateMsgDto } from "../dto/replicate-msg.dto";
import type { HeartBeatResponseDto } from "../dto/heart-beat-response.dto";

export interface LogEntry {
  term: number;
  msg: string;
  commitIndex: number;
}

export interface Vote {
  term: number;
  voteGranted: boolean;
}

interface FollowerHeartbeatResponse {
  followerIndexes: number[] | null;
  follower: string;
}

export class RaftNode {
  term = 0;
  votedFor: string | null = null;
  type = NodeType.follower;
  leader: string | null = null;
  timer = new Timer();
  state: LogEntry[] = [];
  hasQuorum = false;
  quorumRequiredNumber = 1;

  id: string;
  peers: string[];

  constructor(id: string) {
    this.id = id;
    this.peers = PEERS.filter((pid: string) => pid !== this.id);

    this.init();
  }

  public get isLeader(): boolean {
    return this.leader === this.id;
  }

  public get isFollower(): boolean {
    return this.type === NodeType.follower;
  }

  public get messages(): string[] {
    return this.state.map((entry) => entry.msg);
  }

  public get commitIndexs(): number[] {
    return this.state.map((entry) => entry.commitIndex);
  }

  public async submitMsg(msg: string, writeConcern: number): Promise<unknown> {
    const latest = last(this.state);
    const commitIndex = isNumber(latest?.commitIndex)
      ? latest.commitIndex + 1
      : 0;

    this.state.push({
      msg,
      term: this.term,
      commitIndex,
    });

    await this.replicateMsgToFollowers(msg, writeConcern, commitIndex);

    return;
  }

  public replicateMsg(msg: string, latestCommitIndex: number): void {
    const previousCommitIndex = this.state.findIndex(
      (entry: LogEntry) => entry.commitIndex === latestCommitIndex - 1
    );

    const spliceIndex =
      previousCommitIndex >= 0 ? previousCommitIndex + 1 : this.state.length;

    this.state.splice(spliceIndex, 0, {
      msg,
      term: this.term,
      commitIndex: latestCommitIndex,
    });
  }

  private async syncStateWithFollower(
    followerIndexes: number[],
    follower: string
  ): Promise<void> {
    const missedIndexes = differenceBy(
      this.state.map((entry: LogEntry) => entry.commitIndex),
      followerIndexes
    );

    this.state
      .filter((entry: LogEntry) => missedIndexes.includes(entry.commitIndex))
      .forEach(async (entry: LogEntry) => {
        await axios.post<undefined>(
          `http://localhost:${follower}/replicateMsg`,
          {
            msg: entry.msg,
            commitIndex: entry.commitIndex,
          } as ReplicateMsgDto
        );
      });
  }

  private async replicateMsgToFollowers(
    msg: string,
    writeConcern: number,
    latestCommitIndex: number
  ): Promise<void> {
    const concernedRequests: AxiosResponse<undefined, any>[] = [];
    const restRequests: Promise<AxiosResponse<undefined, any>>[] = [];

    this.peers.forEach(async (peer: string, index: number) => {
      try {
        if (size(concernedRequests) < writeConcern - 1) {
          const res = await axios.post<undefined>(
            `http://localhost:${peer}/replicateMsg`,
            {
              msg: msg,
              commitIndex: latestCommitIndex,
            } as ReplicateMsgDto
          );
          concernedRequests.push(res);
        } else {
          restRequests.push(
            axios.post<undefined>(`http://localhost:${peer}/replicateMsg`, {
              msg: msg,
              commitIndex: latestCommitIndex,
            } as ReplicateMsgDto)
          );
        }
      } catch (e: any) {
        console.error(e.code);
      }
    });

    Promise.allSettled(restRequests);
  }

  private init(): void {
    this.timer.start();
    this.timer.on("timeout", () => this.handleTimeout());
  }

  private handleTimeout(): void {
    this.timer.stop();

    this.type = NodeType.candidate;
    this.initVotingProcedure();
  }

  private async initVotingProcedure(): Promise<void> {
    console.log(`[LOG] Voting procedure started by ${this.id}`);

    this.term += 1;
    this.votedFor = this.id;

    const requests = this.peers.map((peer) => {
      console.log("[LOG] Sent voting request to " + peer);

      return axios.post<Vote>(`http://localhost:${peer}/requestVote`, {
        term: this.term,
        candidateId: this.id,
      } as RequestVoteDto);
    });

    const votingResults = await Promise.allSettled(requests);
    const succVotingResults = votingResults.filter(
      (result: PromiseSettledResult<unknown>) => result.status !== "rejected"
    ) as PromiseFulfilledResult<AxiosResponse<Vote, unknown>>[];

    const votedCout = succVotingResults
      .map(
        (votingResult: PromiseFulfilledResult<AxiosResponse<Vote, unknown>>) =>
          votingResult?.value?.data
      )
      .filter(Boolean)
      .reduce((acc, vote: Vote) => {
        return vote.voteGranted ? (acc += 1) : acc;
      }, 0);

    if (
      this.type === NodeType.candidate &&
      votedCout >= this.quorumRequiredNumber
    ) {
      this.type = NodeType.leader;
      this.sendHeartBeats();
    } else {
      console.log("[WARNING]: No quorum to became leader");
      this.hasQuorum = false;
      this.type = NodeType.candidate;
      this.timer.reset();
    }
  }

  private sendHeartBeats(): void {
    setInterval(() => {
      const requests = this.peers.map((peer) => {
        return axios
          .post<HeartBeatResponseDto>(`http://localhost:${peer}/hb`, {
            from: this.id,
            term: this.term,
            leaderId: this.id,
          } as HeartBeatDto)
          .then((response: AxiosResponse<HeartBeatResponseDto, any>) => {
            return {
              followerIndexes: response.data?.commitIndexs,
              follower: peer,
            } as FollowerHeartbeatResponse;
          });
      });

      Promise.allSettled(requests).then(
        (responses: PromiseSettledResult<FollowerHeartbeatResponse>[]) => {
          console.log("[LOG]: HeartBeat SENT");

          responses
            .filter(
              (res: PromiseSettledResult<FollowerHeartbeatResponse>) =>
                res.status === "fulfilled"
            )
            .forEach((res: PromiseSettledResult<FollowerHeartbeatResponse>) => {
              const followerIndexes = (
                res as PromiseFulfilledResult<FollowerHeartbeatResponse>
              ).value.followerIndexes;
              const follower = (
                res as PromiseFulfilledResult<FollowerHeartbeatResponse>
              ).value.follower;

              !isNil(followerIndexes) &&
                this.syncStateWithFollower(followerIndexes, follower);
            });
        }
      );
    }, HEARTBEAT_TIME);
  }
}
