export enum NodeType {
  follower = "follower",
  candidate = "candidate",
  leader = "leader",
}
